# LP2021 Private Cloud workshop 🧊

[![screenshot of recorded workshop video](video-thumbnail.jpg)](https://media.libreplanet.org/u/libreplanet/m/workshop-free-software-data-fortress-for-your-home/)

Supplemental material for a workshop at [LibrePlanet](https://libreplanet.org) 2021: [Free Software Data Fortress For Your Home](https://adammonsen.com/post/1976/) (see also: <https://libreplanet.org/2021/workshops/>).

The purpose of all this is to empower you. With your own private cloud you will be able to ensure freedom for yourself and others.

## Hopes and expections for participants and viewers

* 🚀 Get inspired.
* 🏔️ Feel empowered.
* 🌁 Gain exposure to valuable, self-hostable Free Software.
* 🏛️ Gain knowledge and confidence.

## System overview

* goal: [libre](https://www.gnu.org/philosophy/free-sw.html) from the metal on up
* server operating system: Ubuntu GNU/Linux 20.04 LTS 64-bit server
* operating system configuration: [Ansible](https://www.ansible.com/)
* reverse proxy: [Traefik](https://traefik.io/traefik/)
* runtime strategy: services in containers
    * mostly stand-alone: each service has its own database but our [MTA](https://en.wikipedia.org/wiki/Message_transfer_agent) is shared
* provider (container manager): Docker, via [Docker Compose](https://docs.docker.com/compose/)
* not covered
    * hardware setup (bare metal, virtual machine, etc)
    * operating system install
    * WAN link / networking (other than port forwarding)
    * operating systems besides Ubuntu GNU/Linux
    * reverse proxies besides Traefik
    * providers besides Docker

## Prerequisites

Your _private cloud_ is your data fortress. The private cloud itself is one _server_ (a computer that stays online) running multiple _services_ (distinct long-running programs useful on their own or remotely via other devices). 

You'll need a laptop or desktop computer to create and maintain your private cloud. The following instructions assume Ubuntu GNU/Linux 20.04 LTS 64-bit desktop. You may choose another operating system.

* Ansible version 2.9.6 or later. We'll be running the `ansible-playbook` command.
* An [SSH](https://www.ssh.com/ssh/) client.
* A DNS server for [Let's Encrypt automatic HTTPS certificate generation](https://doc.traefik.io/traefik/https/acme/)
    * This workshop uses Route 53.
    * You may choose from [many DNS providers](https://doc.traefik.io/traefik/https/acme/#providers).
    * You can ignore cert errors (at your own risk) until you are able to get Let's Encrypt working.

## Infrastructure

This section covers required background / back-end to be able to run your "services". These are mostly non-interactive except on occasion.

### Ansible

We'll use [Ansible](https://en.wikipedia.org/wiki/Ansible_(software)) for remote system configuration. With Ansible we create text files declaring the desired state of the system. Ansible performs the actions required to modify the system until the desired state is reached.

The `ansible/` folder in this repository contains Ansible files declaring the desired operating system state. Edit files as necessary and run `ansible-playbook` to update the remote computer.

### Docker Compose

We'll use [Docker Compose](https://docs.docker.com/compose/) to [start and stop containers](https://www.redhat.com/en/topics/containers/what-is-container-orchestration).

#### Basic commands

Start a service. On the server (the computer running Docker), run:

```bash
docker-compose --file ~/ops/wallabag/docker-compose.yml up -d
```

or:

```bash
cd ~/ops/wallabag
docker-compose up -d
```

Tail logs for a service:

```bash
docker-compose --file ~/ops/traefik/docker-compose.yml logs -f
```

### Traefik

[Traefik](https://github.com/traefik/traefik) is the [reverse proxy](https://en.wikipedia.org/wiki/Reverse_proxy) sitting in front of your services. It serves a few unique purposes:

* routing web traffic to the correct service based on hostname
* central management of HTTPS certificates
* enforcement of private cloud global policy, such as to always redirect HTTP requests to HTTPS

### Watchtower

[Watchtower](https://containrrr.dev/watchtower/) is a background service useful for keeping all our other services updated. It automatically downloads new images and restarts containers.

### Dynamic DNS

We use [bshaw/route53-dyndns](https://hub.docker.com/r/bshaw/route53-dyndns) to keep our server's IP address up to date in our DNS server.

### Mail relay

We use [namshi/smtp](https://hub.docker.com/r/namshi/smtp) as a mail relay. This allows services like Nextcloud to send emails.

## Services

This section covers web/mobile software you and your users will interact with regularly.

### Jellyfin

[Jellyfin](https://jellyfin.org/) is a personal streaming media server.

We'll demonstrate setting up a basic Jellyfin server. For DLNA, hardware transcoding and other customizations, see the [installation guide](https://jellyfin.org/docs/general/administration/installing.html#docker).

I like mounting local folders read-only using Nextcloud "external storages" for managing the actual movie and music files.

### Wallabag

[Wallabag](https://www.wallabag.it) saves articles for distraction-free offline reading.

### Nextcloud

[Nextcloud](https://en.wikipedia.org/wiki/Nextcloud) has many apps and forms the core part of our private cloud: file storage, sharing, searching, sorting, groupware (calendar/contacts/chat), maps, rich documents, and much more.

#### Background jobs

Nextcloud must run background jobs regularly to perform maintenance tasks. We use the default (AJAX) method for simplicity, although it can be less reliable. See `/settings/admin` on your Nextcloud server for details on other options.

### Pi-Hole

[Pi-Hole](https://en.wikipedia.org/wiki/Pi-hole) blocks most advertisements and trackers.

## Random tips & tricks

Generate a service password: `openssl rand -hex 32`.

Let's Encrypt cert errors?

* Tail traefik logs (see "Tail logs for a service", above)
* Try restarting traefik as well as the service having cert issues.

Security? Yeah, you need it. Even for a little ol' home server. Some ideas:

* [fail2ban](https://www.fail2ban.org)
* [Zeek open source network security monitoring tool](https://zeek.org)

If your home network is `10.0.0.0/24`, you can use an IP allow list to limit access to certain services by adding this label to the Traefik container:

```
traefik.http.middlewares.myhomeonly.ipwhitelist.sourcerange=10.0.0.0/24
```

In select services, you can then add a label to apply this allow list. Here's an example for the Traefik dashboard:

```
traefik.http.routers.dashboard-https.middlewares=myhomeonly
```

Manual fix for Nextcloud warning *The reverse proxy header configuration is incorrect, or you are accessing Nextcloud from a trusted proxy. If not, this is a security issue and can allow an attacker to spoof their IP address as visible to the Nextcloud. Further information can be found in the [documentation](https://docs.nextcloud.com/server/21/go.php?to=admin-reverse-proxy).* ... You can manually add this stanza to `/data/nextcloud/root/config/config.php`. This is challenging or impossible with Ansible `lineinfile` and `blockinfile`, we might need to use a template instead. Here's the stanza:

```
  'trusted_proxies' =>
  array (
    0 => 'traefik_reverse-proxy_1',
  ),
```

## Ideas

This section is for ideas on how this documentation and code could be improved. Patches welcome!

- [ ] add glossary
- [ ] add info on backups
- [ ] add recommendations for monthly maintenance
- [ ] include bare-metal bootstrapping: e.g. how to plug in actual hardware and install the operating system
- [ ] more [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself): only define timezone once, or use UTC; also user IDs like those for jellyfin (see <https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#tips-on-where-to-set-variables>)

## Non-goals

* Enterprise-ready private cloud.
    * This solution should perform well with up to 10 users.
    * This is not hardened or ready to be scaled up or scaled out.
* Get it perfect the first time.
    * This is a journey! It's OK if it doesn't work exactly as you'd hoped today.
    * "Don't worry about losing. If it's right, it happens. The main thing is not to hurry. Nothing good gets away." -- John Steinbeck
* Generic, pluggable solution that works with any operating system / provisioning system / reverse proxy / network topology.
    * This workshop assumes Ubuntu GNU/Linux, Ansible, Traefik. See "System overview", below.
    * This workshop *should* be *hardware agnostic*.
    * Any home network should work assuming you can forward traffic on port 443 to your server.

## To do

- [x] add copyright and license
- [ ] add sections / table of contents
- [ ] add diagrams
    - [ ] architecture: OS, Docker, services
    - [ ] network traffic: cloud, WAN, LAN
- [ ] introduce the concept of "monthly maintenance"
    - [ ] apply updates, reboot server
- [x] update Ansible configuration files
- [x] run and re-run Ansible
- [x] create initial services
    - [x] jellyfin
    - [x] wallabag
    - [x] nextcloud
    - [x] encapsulate data backends (e.g. create Wallabag's mariadb in same `docker-compose.yml` as web app)
- [x] destroy and re-create server and services
- [x] stand up shared mail service
- [x] nextcloud CSP errors 
    - [x] document below, mention https://help.nextcloud.com/t/some-http-links-not-rewritten-to-https-despite-overwriteprotocol-https/74192 OR
    - [x] automate `/data/nextcloud/root/config/config.php` fix in ansible
- [ ] resolve FIXMEs in all source code and config files
- [x] push code to gitlab
- [x] add dyndns
- [x] add watchtower
- [ ] automate `time docker-compose --file ~/ops/traefik/docker-compose.yml up -d` and friends
- [ ] implement /data as a ZFS filesystem
- [ ] add initial backup scripts / instructions
- [ ] time permitting: look into FOSS single sign-on (authelia, keycloak, uaa, etc)
- [ ] get collabora or onlyoffice running (nextcloud online collaborative rich document editor)
- [ ] add glossary coordinated with diagrams
    - [ ] operating system (Ubuntu GNU/Linux)
    - [ ] reverse proxy (Traefik)
    - [ ] provisioning system (Ansible)
    - [ ] network topology
- [ ] address nextcloud security & setup warnings
    - [x] The reverse proxy header configuration is incorrect, or you are accessing Nextcloud from a trusted proxy. If not, this is a security issue and can allow an attacker to spoof their IP address as visible to the Nextcloud. Further information can be found in the [documentation](https://docs.nextcloud.com/server/21/go.php?to=admin-reverse-proxy).
    - [x] The "Strict-Transport-Security" HTTP header is not set to at least "15552000" seconds. For enhanced security, it is recommended to enable HSTS as described in the [security tips](https://docs.nextcloud.com/server/21/go.php?to=admin-security).
    - [x] Your installation has no default phone region set. This is required to validate phone numbers in the profile settings without a country code. To allow numbers without a country code, please add "default_phone_region" with the respective [ISO 3166-1 code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements) of the region to your config file.
    - [ ] Module php-imagick in this instance has no SVG support. For better compatibility it is recommended to install it.

## Copyright and license

This material is ©2021 Adam Monsen <mailto:haircut@gmail.com>

You can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License or (at your option) any later version.
