#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

secretsFile=secrets/all-secrets.sh

if ! [[ -r $secretsFile ]]
then
    echo "You don't have a default secrets file. I'll create one for you now."
    echo "Edit $secretsFile with working values then re-run this script."
    cp secrets/secrets-template $secretsFile
    exit 1
fi

source $secretsFile

set -x

ansible-playbook myplaybook.yml
