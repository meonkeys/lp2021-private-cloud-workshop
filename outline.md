# Talk outline

## Intros

* Welcome!
* what's your name
* where are you
* what brings you here
* sysadmin/devops knowledge: beginner, intermediate, advanced?

## Please

* Glossary - Patches welcome!
* LP2021 safe space policy https://libreplanet.org/2021/safe-space-policy/
* No winning or losing, we're a team

see README.md
